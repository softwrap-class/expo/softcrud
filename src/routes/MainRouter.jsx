import React, { useState, useContext } from 'react';
import { Text, View } from 'react-native';
import Logged from '../pages/Logged';
import Login from '../pages/Login';
import Register from '../pages/Register';
import { StatusBar } from 'expo-status-bar';
import { AuthContext } from '../context/AuthContext';
import Profile from '../pages/Profile';

const MainRouter = () => {
  const [currentPage, setCurrentPage] = useState('Login');
  const [loggedPage, setLoggedPage] = useState('Dashboard');
  const { loading, loggedUser, userData } = useContext(AuthContext);

  const renderComponent = () => {
    if (loading) {
      return <Text>Carregando...</Text>;
    } else {
      if (loggedUser) {
        switch (loggedPage) {
          default:
          case 'Dashboard':
            return (
              <Logged
                onProfileClick={() => setLoggedPage('Profile')}
              />
            );
          case 'Profile':
            return (
              <Profile
                onDashboardClick={() => setLoggedPage('Dashboard')}
              />
            );
        }
      } else {
        switch (currentPage) {
          default:
          case 'Login':
            return (
              <Login
                onRegisterClick={() => setCurrentPage('Register')}
              />
            );
          case 'Register':
            return (
              <Register
                onLoginClick={() => setCurrentPage('Login')}
              />
            );
        }
      }
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <StatusBar style="auto" />
      {renderComponent()}
    </View>
  );
};

export default MainRouter;
