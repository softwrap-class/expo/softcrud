import React from 'react';
import { Formik } from 'formik';
import { View, Alert } from 'react-native';
import DefaultInput from '../components/DefaultInput';
import { Button } from 'react-native-paper';
import Header from '../components/Header';
import { fireAuth } from '../config/firebase';

const Login = ({ onRegisterClick }) => {
  const loginUser = async (values) => {
    if (!values.email || !values.password) {
      Alert.alert('Atenção', 'Preencha todos os dados');
      return;
    }

    try {
      await fireAuth.signInWithEmailAndPassword(
        values.email,
        values.password
      );
    } catch (e) {
      Alert.alert('Erro !', 'Algo deu errado');
      console.log(e);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title="Login"
        subtitle="Digite suas informações para entrar"
      />
      <Formik
        initialValues={{ email: '' }}
        onSubmit={(values) => loginUser(values)}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View style={{ padding: 15 }}>
            <DefaultInput
              required
              label="E-Mail"
              keyboardType="email-address"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              autoCapitalize="none"
            />
            <DefaultInput
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              required
              secureTextEntry
              label="Senha"
            />
            <Button
              icon="camera"
              mode="outlined"
              onPress={handleSubmit}
            >
              Entrar
            </Button>
            <Button
              icon="camera"
              mode="outlined"
              onPress={onRegisterClick}
            >
              Registrar
            </Button>
          </View>
        )}
      </Formik>
    </View>
  );
};

export default Login;
