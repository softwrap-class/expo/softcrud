import React from 'react'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'

const DefaultButton = ({ label, onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
      <LinearGradient colors={['#0093E9', '#80D0C7']} style={styles.button}>
        <Text
          style={{
            backgroundColor: 'transparent',
            fontWeight: 'bold',
            fontSize: 15,
            color: '#fff',
          }}
        >
          {label && label.toUpperCase()}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    marginTop: 10,
    borderRadius: 5,
    marginBottom: 5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default DefaultButton
