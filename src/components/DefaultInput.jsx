import React, { Fragment } from 'react'
import { HelperText, TextInput } from 'react-native-paper'

const DefaultInput = ({
  label,
  name,
  required,
  ...rest
}) => {
  return (
    <Fragment>
      <TextInput
        label={label}
        mode="outlined"
        {...rest}
      />
      <HelperText type="error">
      </HelperText>
    </Fragment>
  )
}

export default DefaultInput
