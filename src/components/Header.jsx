import { Appbar } from 'react-native-paper';
import React from 'react'

const Header = ({ goBack, handleSearch, handleMore, title, subtitle }) => {
  return (
    <Appbar.Header>
      {
        !!goBack && typeof goBack === 'function' && (
          <Appbar.BackAction onPress={goBack} />
        )
      }
      <Appbar.Content title={title} subtitle={subtitle} />
      {
        !!handleSearch && typeof handleSearch === 'function' && (
          <Appbar.Action icon="magnify" onPress={handleSearch} />
        )
      }
      {
        !!handleMore && typeof handleMore === 'function' && (
          <Appbar.Action icon="dots-vertical" onPress={handleMore} />
        )
      }
    </Appbar.Header>
  );
}

export default Header;