import { useState, useEffect } from 'react'
import _ from 'lodash'

const useInputHandler = () => {
  const [errors, setErrors] = useState({})
  const [dataValid, setDataValid] = useState(false)
  const [values, setValues] = useState({})

  const initializeInput = (name) => {
    updateInputValue(name, '')
  }

  useEffect(() => {
    const hasAllValues = Object.values(values).every(value => !!value);

    if (hasAllValues && Object.keys(errors).length === 0) {
      setDataValid(true);
    } else {
      setDataValid(false);
    }
  }, [errors, values])

  const updateInputValue = (name, value) => {
    const newValues = _.cloneDeep(values)
    newValues[name] = value

    setValues(newValues)
  }

  const addError = (name, error) => {
    const newErrors = _.cloneDeep(errors)
    newErrors[name] = error

    setErrors(newErrors)
  }

  const removeError = (name) => {
    const newErrors = _.cloneDeep(errors)
    delete newErrors[name]

    setErrors(newErrors)
  }

  return [
    {
      addError,
      removeError,
      updateInputValue,
      initializeInput,
      values,
      errors,
    },
    values,
    dataValid,
  ]
}

export default useInputHandler
