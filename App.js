import React from 'react';
import './src/config/firebase';
import { Provider as PaperProvider } from 'react-native-paper';
import MainRouter from './src/routes/MainRouter';
import { AuthProvider } from './src/context/AuthContext';

const App = () => {
  return (
    <PaperProvider>
      <AuthProvider>
        <MainRouter />
      </AuthProvider>
    </PaperProvider>
  );
};

export default App;
